package models;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import siena.DateTime;
import siena.Generator;
import siena.Id;
import siena.Model;


public class Tweet extends Model {

	@Id(Generator.AUTO_INCREMENT)
	public Long id;       

	@Required
	@MaxSize(140)
	public String tweet;

	@Required
	@DateTime
	public Date createDate = new Date();
	
	public String toString() {
		return tweet;
	}

	public static List<Tweet> findLatest() {
		return Model.all(Tweet.class).order("-createDate").fetch();
	}
}