package controllers;

import java.util.List;

import models.Tweet;
import play.cache.Cache;
import play.mvc.After;
import play.mvc.Before;
import play.mvc.Controller;
import siena.Model;
import siena.Query;

public class Application extends Controller {
	static final int MAX = 5000;
	
	static Query<Tweet> q;
//	static Integer count = 0;

	@Before
	public static void loadQuery() {
		q = (Query<Tweet>) Cache.get("q"+session.getId());
		if (q == null) {
			// stateful is just meant to use GAE cursors instead of offsets
			q = Model.all(Tweet.class).order("-createDate").paginate(10);
		}
	}

	@After
	public static void saveQuery() {
		Cache.set("q"+session.getId(), q);
	}

	public static void index() {
		List<Tweet> tweets = (List<Tweet>)Cache.get("current"+session.getId());
		if(tweets == null){
			tweets = q.fetch();
			Cache.set("current"+session.getId(), tweets, "10mn");
		}

		render(tweets);
	}

	public static void currentPage() {
		List<Tweet> tweets = (List<Tweet>)Cache.get("current"+session.getId());
		if(tweets == null){
			tweets = q.fetch();
			Cache.set("current"+session.getId(), tweets, "10mn");
		}
		renderTemplate("Application/list.html", tweets);
	}
	
	public static void count() {
		Integer count = (Integer)Cache.get("count");
		if(count == null){
			count = Model.all(Tweet.class).count();
			Cache.set("count", count, "10mn");
		}
		renderTemplate("Application/count.html", count);
	}
	
	public static void nextPage() {
		List<Tweet> tweets = q.nextPage().fetch();
		Cache.safeReplace("current"+session.getId(), tweets, "10mn");

		renderTemplate("Application/list.html", tweets);
	}

	public static void previousPage() {
		List<Tweet> tweets = q.previousPage().fetch();
		Cache.safeReplace("current"+session.getId(), tweets, "10mn");

		renderTemplate("Application/list.html", tweets);
	}

	public static void create(String msg) {
		Tweet tweet = new Tweet();
		tweet.tweet = msg;
		tweet.save();
		
		// releases query context
		q.release();
		// delete current page from list
		Cache.delete("current"+session.getId());
		Cache.delete("count");
		renderTemplate("Application/create.html", tweet);
	}

	public static void clear() {
		Model.all(Tweet.class).delete();
		index();
	}
}